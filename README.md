# MyProjects

## Description

Static HTML website presenting a collection of my projects. The development environment was set up with Gulp. Following plugins are installed:

* browser-sync
* gulp-stylelint
* gulp-htmlmin
* gulp-html-replace
* gulp-concat-css
* gulp-autoprefixer
* gulp-clean-css
* gulp-imagemin
* gulp-babel

## Installation

Installing required packages:

* npm install

## Gulp commands

* glup - launches server ( http://localhost:3000/ ) and automatically reloads page whenever html, css of js files change
* gulp stylelint - runs stylelint and displays errors in the terminal
* gulp build - creates production files and places them in the ./build folder (minifies html and replaces stylesheet links, concats css files and adds prefixes, transpiles js)
* gulp build-server - launches server and uses files from ./build folder to display the website
