var gulp = require('gulp')
var browserSync = require('browser-sync').create()
var reload = browserSync.reload
var gulpStylelint = require('gulp-stylelint')
var concatCss = require('gulp-concat-css')
var autoprefixer = require('gulp-autoprefixer')
var htmlReplace = require('gulp-html-replace')
var cleanCss = require('gulp-clean-css')
var htmlMin = require('gulp-htmlmin')
var imageMin = require('gulp-imagemin')
var babel = require('gulp-babel')



gulp.task('server', function() {
  browserSync.init({
    server: {
      baseDir: './',
    },
  })
})

gulp.task('build-server', function() {
  browserSync.init({
    server: {
      baseDir: './build',
    },
  })
})

gulp.task('stylelint', function lintCssTask() {
  gulp
    .src('css/*.css')
    .pipe(gulpStylelint({
      reporters: [
        { formatter: 'string', console: true },
      ],
    }))
})

gulp.task('build', function() {
  gulp
    .src('index.html')
    .pipe(htmlReplace({
      css: 'css/style.min.css',
    }))
    .pipe(htmlMin({
      collapseWhitespace: true,
    }))
    .pipe(gulp.dest('./build'))
  gulp.src('css/*.css')
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false,
    }))
    .pipe(concatCss('style.min.css'))
    .pipe(cleanCss({
      compatibility: 'ie8',
    }))
    .pipe(gulp.dest('./build/css'))
  gulp.src('js/main.js')
    .pipe(babel({
        presets: ['env']
    }))
    .pipe(gulp.dest('build/js'))
  gulp
    .src('./img/*')
    .pipe(imageMin())
    .pipe(gulp.dest('build/img'))
})

gulp.task('watch', function() {
  gulp.watch('*.html').on('change', reload)
  gulp.watch('*/*.css').on('change', reload)
  gulp.watch('*/*.js').on('change', reload)
})

gulp.task('default', ['server', 'watch'])
