window.dataLayer = window.dataLayer || []
function gtag(){dataLayer.push(arguments)}
gtag('js', new Date())
gtag('config', 'UA-107061016-1')

var trackOutboundLink = function(url) {
  gtag('event', 'click', {
    'event_category': 'outbound',
    'event_label': url,
    'transport_type': 'beacon'
  })
}

window.onload = () => {
  document.getElementById('rs-react').addEventListener('click', () => {
    trackOutboundLink('http://mcencek.gitlab.io/rsReact/')
    return false
  })

  document.getElementById('actuality').addEventListener('click', () => {
    trackOutboundLink('http://actuality.pl/')
    return false
  })

  document.getElementById('geosurvey-app').addEventListener('click', () => {
    trackOutboundLink('http://www.recoded.co/geoankieta-2/')
    return false
  })

  document.getElementById('geosurvey-landing').addEventListener('click', () => {
    trackOutboundLink('http://rynki.geoankieta.pl/start')
    return false
  })

  document.getElementById('r45').addEventListener('click', () => {
    trackOutboundLink('http://r45.pl/')
    return false
  })
}
